import json


class IterJsonReader:
    def __init__(self, open_read_element):
        self.text = open_read_element

    def __next__(self):
        text = self.text.readline()
        if text:
            return json.loads(text)
        else:
            raise StopIteration

    def __iter__(self):
        return self
