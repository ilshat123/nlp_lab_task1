import json

from iter_json_reader import IterJsonReader


class TypeObj:
    def __init__(self, name):
        self.name = name
        self.relations_num = 0
        self.max_distance = 0

        self.all_words_num = 0

    @property
    def middle_distance(self):
        return self.all_words_num / self.relations_num

    def add_obj(self, element):
        self.all_words_num += len(element.get('middle_context'))
        self.relations_num += 1
        self.max_distance = max(self.max_distance, len(element.get('middle_context')))

    def __str__(self):
        return '{0}({1})'.format(self.__class__.__name__, self.__dict__)

    __repr__ = __str__


def create_type_objs(elements):
    types_list = {}
    for elem in elements:
        elem_type = elem['type']
        type_elem_obj = types_list.get(elem_type, TypeObj(elem_type))
        type_elem_obj.add_obj(elem)
        types_list[elem_type] = type_elem_obj
    return types_list


def get_files(filename, is_iterable_obj=True):
    if is_iterable_obj:
        # Чтобы не загружать весь файл в память
        files = IterJsonReader(open(filename, 'r', encoding='utf-8'))
        return files
    else:
        with open(filename, 'r', encoding='utf-8') as file:
            files = file.read().split('\n')
            files = [json.loads(elem) for elem in files if elem != '']
            return files


def write(filename, text):
    file = open(filename, 'w', encoding='utf-8')
    file.write(text)
    file.close()


if __name__ == '__main__':
    filename = "positive_relations.txt"
    result_txt_name = "results.txt"
    files = get_files(filename, is_iterable_obj=True)

    result = create_type_objs(files)
    all_obj = TypeObj("all")
    all_obj.max_distance = max([elem.max_distance for elem in result.values()])
    all_obj.relations_num = sum([elem.relations_num for elem in result.values()])
    all_obj.all_words_num = sum([elem.all_words_num for elem in result.values()])
    result[all_obj.name] = all_obj

    result_lines = ['{}\t{}\t{}\t{}'.format(elem.name, elem.relations_num, elem.middle_distance, elem.max_distance)
                    for elem in result.values()]

    print('\n'.join(result_lines))

    for i in result.values():
        print(i)

    write(result_txt_name, '\n'.join(result_lines))


